def get_terms(poly_str):
    return [(int(f"{thing[0]}{re.sub(r'^$', '1', thing[1])}"), int('0' if thing[4] == '' and thing[2] == '' else thing[4] if thing[4] != '' else '1')) for thing in re.findall(r'([+-]*)(\d+)?(x(\^(\d+))?)?', poly_str)[:-1]]

    if thing[4] == '' and thing[2] == '':
        return int('0')
    else:
        if thing[4] != '':
            return int(thing[4])
        else:
            return int('1')
