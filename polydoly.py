import re


def get_terms(polystr):
    """
      >>> get_terms('3x^5-7x^2+11')
      [(3.0, 5), (-7.0, 2), (11.0, 0)]
      >>> get_terms('x^3-x^2+x')
      [(1.0, 3), (-1.0, 2), (1.0, 1)]
    """
    regex = re.compile(r"([+-]*)(\d+)?(x(\^(\d+))?)?")
    raw_terms = regex.findall(polystr)[:-1]
    terms = []

    # extract terms from capture groups
    for raw_term in raw_terms:
        # handle exponent of constant term
        if not raw_term[-1] and not raw_term[2]:
            exp = 0
        # handle exponent of linear term
        elif not raw_term[-1]:
            exp = 1
        else:
            exp = int(raw_term[-1])

        # handle coefficient of 1
        if not raw_term[1]:
            coeff = 1.0
        else:
            coeff = float(raw_term[1])
        # handle negative coefficients
        if raw_term[0] == '-':
            coeff *= -1

        terms.append((coeff, exp))

    return terms


class Polynomial:
    """
      >>> p = Polynomial('3x^2-7x+9')
      >>> print(p)
      3.0x^2 - 7.0x + 9.0
    """
    def __init__(self, poly_str):
        self.terms = get_terms(poly_str)

    def __str__(self):
        poly_str = ''

        for term in self.terms:
            coeff, exp = term
            coeff_str = ''
            # handle negative coefficents
            if coeff < 0:
                coeff_str += ' - '
                coeff *= -1
            else:
                coeff_str += ' + '
            # handle coefficents of 1
            if coeff != 1:
                coeff_str += str(coeff)

            exp_str = ''
            # ignore exp_str for constant term, handle linear term
            if exp > 1:
                exp_str += f'x^{exp}'
            elif exp == 1:
                exp_str += 'x'

            poly_str += coeff_str + exp_str

        return poly_str[3:] if poly_str[1] == '+' else '-' + poly_str[3:]


if __name__ == '__main__':
    import doctest
