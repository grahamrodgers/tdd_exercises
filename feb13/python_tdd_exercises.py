def reverse_list(lst):
    """
    Reverses order of elements in list lst.
    """
    reversed = []
    for item in lst:
        reversed = [item] + reversed
    return reversed

def reverse_string(s):
    return s[::-1]

def is_english_vowel(self):
    if(self == 'a' or self == 'e' or self == 'i' or self == 'o' or self == 'u' or self == 'y' or self == 'A' or self == 'E' or self == 'I' or self == 'O' or self == 'U' or self == 'Y'):
        return True 
    else:
        return False

def count_num_vowels(s):
    lowercase = s.lower()
    vowel_list = {}
    for vowel in s:
        count = lowercase.count(vowel)
        vowel_list[vowel] = count
    return(count)

def histogram(l):
    item = ""
    for n in item:
        final = ''
        morehis = n
        while(morehis > 0):
            final += '*'
            morehis = morehis -1
        return final

def get_word_lengths(s):
    wl = list(map(len, s.split()))
    return wl

def find_longest_word(s):
    longest = ''
    for word in s:
        if len(word) > len(longest)
            longest = word
    return longest

def validate_dna(s):
    for i in dna:
        if not i in 'actgACTG':
            return False
        else:
            return True

def base_pair(c):
    pairs = {"a": "t", "t": "a", "c": "g", "g": "c"}
    if c in dnamap:
        return dnamap[c]
    else:
        return 'Unknown'

def transcribe_dna_to_rna(s):
    return s.upper().replace('T', 'U')

def get_complement(s):
    pairs = {"A": "T", "T": "A", "C": "G", "G": "C"}
    return ''.join([pairs[ch] for ch in s.upper()])
