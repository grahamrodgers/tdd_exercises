import re

class Polynomial:

    def __init__(self, polystr):
	self.int = get_terms(polystr)

    def get_terms(polystr):

    	"""
      	>>> get_terms('3x^5-7x^2+11')
      	[(3.0, 5), (-7.0, 2), (11.0, 0)]
     	>>> get_terms('x^3-x^2+x')
      	[(1.0, 3), (-1.0, 2), (1.0, 1)]
   	 """

    	regex= re.compile(r"([+-]*) *(\d+)?(x(\^(\d+))? *)?")
    	indv = regex.findall(polystr)
    	int = []

    	for num in indv:
        	if num[1]:
            	coefficient = float(i[1])
        	else:
            	coefficient = 1

        	if num[0] == '-':
            	coefficient = -coefficient

        	if num[2]:
            	if i[4]:
                	exponent = int(i[4])
           	else:
                	exponent = 1
        	else:
            	exponent = 0
	
        	int.append((coefficient, exponent))

    	return int

if __name__ == '__main__':
    import doctest
    doctest.testmod()
