from unittest import TestCase

from polynomial_smelter import Polynomial

class PolynomialTestSuite(TestCase):
    def setUp(self):
        self.p1 = Polynomial()

    def test_default_polynomial_is_0(self):
        self.assertEqual(str(self.p1), '0')

    def test_create_constant_polynomials(self):
        self.p2  = Polynomial('42')
        self.assertEqual(str(self.p2), '42')
    
    def test_polynomial_prints_nicely(self):
        self.p1 = Polynomial('2x^2 + 2x + 2')
        self.p2 = Polynomial('7x^3+4x^2+x')
        self.assertEqual(str(self.p1), '2x^2 + 2x + 2')
        self.assertEqual(str(self.p2), '7x^3 + 4x^2 + x')
